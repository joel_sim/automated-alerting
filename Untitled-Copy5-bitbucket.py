#!/usr/bin/env python
# coding: utf-8

# general packages
import os
import datetime as DT
import pandas as pd
import numpy as np
from IPython.display import display # display multiple df in a cell
import warnings
warnings.simplefilter('ignore')

# modelling packages
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors
from sklearn import metrics
from kneed import KneeLocator 

# retrieve data from GBQ
from google.cloud import bigquery 
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'C:/Users/josim/AppData/Roaming/gcloud/application_default_credentials.json'

#%load_ext google.cloud.bigquery
#%%reload_ext google.cloud.bigquery

project_id = 'dyson-quality-sandbox'

client = bigquery.Client(project = project_id)

# export to GBQ
import pandas_gbq

# ### Data Extraction (BigQuery)
def download_data(prev_date, curr_date, limit_date):
    print("Download Start Time: ", DT.datetime.today())

    # Query for Big Query
    QUERY = """
    SELECT part, sum(Quantity) as QTY
    FROM
    `dyson-gds-ser-rddqrc-91a0.RIS.Interface_Jobs` J
    INNER JOIN
    (SELECT Job_Week, ROW_NUMBER() OVER(order by job_week desc) as Counter
    FROM `dyson-gds-ser-rddqrc-91a0.RIS.Interface_Jobs`
    group by job_week) W
    ON J.job_week = w.job_week
    Where 
    W.counter = 1
    and Is_Failure_Candidate = 'Yes'
    Group by
    Part
    Having QTY > 50
    Order by QTY desc
    """

    # run the query and get the data from Big Query
    query_job = client.query(QUERY)
    data = query_job.to_dataframe()
    #filename = 'part-usage-bb-'+curr_date+'.csv'
    #data.to_csv(filename, index=False)

    # retrieve and archive the data
    data['part2'] = data['part'].str.split(':').str[0]
    #archive_parts = data['part2'].tolist()
    data['part3'] = "'"+data['part'].str.split(':').str[0]+"'"

    archive_parts = data['part3'].tolist()
    archive_parts = '('+','.join(archive_parts)+')'

    run_parts = data[data['QTY'] > 100]['part2'].tolist()

    # archive data and save it in GCP BQ table
    ARCHIVE_QUERY = """
    SELECT GQR_Primary_Function,
    GQR_Model,
    GQR_Project,
    GQR_Range,
    GQR_Generation,
    Serial_Number_Batch,
    Serial_Number_Country,
    Manufacturer,
    Build_Month,
    Batch_Size,
    Simple_Part_Number,
    Simple_Part_Name,
    Months_From_Purchase,
    GQR_Months_From_Purchase_Band,
    Qty,
    Job_Key_Count
    FROM `dyson-gds-ser-rddqrc-91a0.RIS.ASSET_Batch_Cost_Report_AGG`
    WHERE Simple_Part_Number IN {}
    AND Build_Month >= {}
    AND Manufacturer IS NOT NULL
    ORDER BY Build_Month
    """.format(archive_parts, limit_date)

    query_job = client.query(ARCHIVE_QUERY)
    archive = query_job.to_dataframe()

    archive['Archive_Date'] = pd.to_datetime(curr_date)

    table_id = 'Joel.EWPOC_Archive_BB'
    pandas_gbq.to_gbq(archive, table_id, project_id=project_id, if_exists='append')
    pandas_gbq.to_gbq(archive, table_id+"_"+curr_date, project_id=project_id, if_exists='replace')

    # Query for Big Query (prev-data)
    QUERY_PREV = """
    SELECT * FROM `dyson-quality-sandbox.Joel.EWPOC_Archive` 
    WHERE Archive_Date = '{}'
    """.format(prev_date)

    # run the query and get the data from Big Query
    query_job_prev = client.query(QUERY_PREV)
    data_prev = query_job_prev.to_dataframe()

    print("Download End Time: ", DT.datetime.today())

    return data_prev, archive, archive, run_parts


# calculate the AR_diff
def get_ar_diff(prev_df, curr_df, m):
    prev_df['Build_Month'] = pd.to_datetime(prev_df['Build_Month'])
    curr_df['Build_Month'] = pd.to_datetime(curr_df['Build_Month'])
    
    # to account for actual m-mth AR
    up_limit_year = DT.date.today().year
    up_limit_mth = DT.date.today().month - m
    
    # Month to Fail > 12 mth
    if (up_limit_mth <= -12): 
        up_limit_mth = 24 + up_limit_mth
        up_limit_year = up_limit_year - 2
    
    # Month to Fail < 12 mth
    elif (up_limit_mth <= 0 and up_limit_mth > -12):
        up_limit_mth = 12 + up_limit_mth
        up_limit_year = up_limit_year - 1

    up_limit_date = "01-"+str(up_limit_mth)+"-"+str(up_limit_year)
    
    prev_df = prev_df[prev_df['Build_Month'] <= up_limit_date]
    curr_df = curr_df[curr_df['Build_Month'] <= up_limit_date]
    
    # get the AR_diff
    prev_df.rename(columns = {'AR': ('AR_prev')}, inplace = True)
    curr_df.rename(columns = {'AR': ('AR_today')}, inplace = True)

    # adding in manufacturer because there are duplicates - 1 batch has multiple CM LOL
    # keeping the columns names from prev_data (if prev_df has that info, curr_df should have)
    df = prev_df.merge(curr_df, how='outer', on=['Serial_Number_Batch', 'Months_From_Purchase', 'Manufacturer'], suffixes=('_x', ''))
    df.drop(df.filter(regex='_x$').columns.tolist(),axis=1, inplace=True)
    df['AR_diff'] = df['AR_today'] - df['AR_prev']
    
    # replace all negative AR-diff to 0 - to improve on clustering
    df['AR_diff'][df['AR_diff'] < 0] = 0
    
    # there is a case where prev dataset has data but curr dataset has no data - remove them (outliers)
    df.dropna(subset=['Qty'], inplace=True)
    
    df.fillna(0, inplace=True)

    return df


# df is the dataframe returned from base_model() 
# x1 & x2 are 2 variables used to train the model 
def train_dbscan(df):
    x1 = 'AR_diff'
    x2 = 'AR_diff'
    
    if(len(df) > 1):
        # finding epsilon and min points
        neigh = NearestNeighbors(n_neighbors=2)
        nbrs = neigh.fit(df[[x1, x2]])    
        distances, indices = nbrs.kneighbors(df[[x1, x2]])

        # find the distance
        distances = np.sort(distances, axis=0)
        distances = distances[:,1]
        
        # finding the optimal eps using knee locator 
        kneedle = KneeLocator(range(len(distances)),distances, S=1.0, curve="convex", direction="increasing")

        try:
            opt_eps = (round(kneedle.knee_y, 4))
        except:
            opt_eps = 0.0001
        
        # eps cannot be <= 0
        if (opt_eps <= 0):
            opt_eps = 0.0001

        # eps is determine based on the k-distance graph, min_samples (minpts) is determine based on heuristic idea (ln(number of data pts))
        min_samp = max(np.log(len(distances)), 3)
        
        dbscan_opt = DBSCAN(eps=opt_eps, min_samples=min_samp) 

        dbscan_opt.fit(df[[x1,x2]])
        df['Label'] = dbscan_opt.labels_
        
        # A metric used to calculate the goodness of a clustering technique. Ranges from -1 to 1. 
        # 1: Means clusters are well apart from each other and clearly distinguished.
        # if there is only 1 cluster, set sil_score to 100
        if(len(df['Label'].unique()) < 2):
            sil_score = 100
        else:
            sil_score = metrics.silhouette_score(df[[x1,x2]], df['Label'])
        
    else:
        df['Label'] = 0
        sil_score = 100
        
    # batches flagged out by DBSCAN
    flagged_df = df[df['Label'] < 0]

    # drop duplicated column name
    df = df.loc[:,~df.columns.duplicated()]
    flagged_df = flagged_df.loc[:,~flagged_df.columns.duplicated()]
    
    # set non-outliers to 0 regardless of their clusters, and 1 as outlier
    df['Label'][df['Label'] > 0] = 0
    df['Label'][df['Label'] < 0] = 1
    
    return df, flagged_df, sil_score


def main_fn(part_list, prev_data_all, curr_data_all, curr_date):
    ### CREATE an empty dataframe for results v4 (add in part)
    resultdf3 = pd.DataFrame(columns=['Date', 'GQR_Primary_Function', 'GQR_Model', 'GQR_Project', 'GQR_Range', 'GQR_Generation', 'Serial_Number_Batch', 'Serial_Number_Country', 'Manufacturer', 'Build_Month', 'Batch_Size', 'Simple_Part_Number', 'Simple_Part_Name', 'Months_From_Purchase', 'GQR_Months_From_Purchase_Band', 'Label'])
    sil_score_df = pd.DataFrame(columns=['Part', 'Date', 'Month to Fail', 'Score'])
    sil_score = 0
    file_not_found=['sample']
    
    # loop through all the parts
    for part in part_list:
        
        prev = prev_data_all[prev_data_all['Simple_Part_Number'] == part]
        curr = curr_data_all[curr_data_all['Simple_Part_Number'] == part]

        # loop through all months to fail (from 1 to 24)
        for mth in range(1,25):

            # normal operations
            if(len(prev)>0):                
                prev_data = prev[prev['Months_From_Purchase'] == mth]
                curr_data = curr[curr['Months_From_Purchase'] == mth]
                
                prev_data['AR'] = prev_data['Qty']/prev_data['Batch_Size']
                curr_data['AR'] = curr_data['Qty']/curr_data['Batch_Size']

                curr_df = get_ar_diff(prev_data, curr_data, mth)

                dbscan_df, flagged_df, sil_score = train_dbscan(curr_df)
                
                score_df = pd.DataFrame({'Part': part,
                                        'Date': curr_date, 
                                        'Month to Fail': mth, 
                                        'Score': sil_score}, 
                                       columns = ['Part', 'Date', 'Month to Fail', 'Score'], 
                                       index=[0])

                sil_score_df = pd.concat([sil_score_df, score_df], ignore_index=True)
                sil_score_df['Date'] = pd.to_datetime(sil_score_df['Date']).apply(lambda a: pd.to_datetime(a).date())
                
                dbscan_df['Date'] = curr_date
                dbscan_df['Date'] = pd.to_datetime(dbscan_df['Date']).apply(lambda a: pd.to_datetime(a).date())

                resultdf3 = pd.concat([resultdf3, dbscan_df], ignore_index=True)

            # if parts were not archived in prev data
            else:
                file_not_found.append(part+"-"+str(mth)+"mth")
                #print(part + ' not found in previous data.')

                
    resultdf3['Build_Month'] = pd.to_datetime(resultdf3['Build_Month'], utc=True).apply(lambda a: pd.to_datetime(a).date())
    resultdf3['Date'] = pd.to_datetime(resultdf3['Date'], utc=True).apply(lambda a: pd.to_datetime(a).date())
    
    ### SAVE into excel (1 file for all parts) ###
    #output_file_name = 'part usage detection-bb-'+curr_date
       
    file_not_found_df = pd.DataFrame(file_not_found, columns=['File Not Found'])
    #file_not_found_df.to_csv(output_file_name+'-filenotfound.csv', index=False)
    
    # calculating the score
    resultdf3['Score'] = resultdf3['Label'] * resultdf3['AR_diff'] * resultdf3['Batch_Size']
    #resultdf3.to_csv(output_file_name+'-reformat.csv', index=False)
    #print('Exported to Excel!')
    
    table_id = 'Joel.EWPOC_Results_BB_'+curr_date

    # today's result only
    pandas_gbq.to_gbq(resultdf3, table_id, project_id=project_id, if_exists='replace')
    print('Exported to GBQ!')
    
    # combine all existing results and current result into 1 table 
    pandas_gbq.to_gbq(resultdf3, "Joel.EWPOC_Results_BB", project_id=project_id, if_exists='append')
    print('Combined all data in GBQ!')
    
    print("\n*********** ALL RESULTS EXPORTED ***********\n")
    

def main():
    print("Start Time: ", DT.datetime.today())
    
    prev_date=str((DT.date.today() - DT.timedelta(days=7)))
    curr_date=str(DT.date.today())

    # 36-mth duration max from today
    limit_date="'"+str((DT.date.today() - DT.timedelta(days=365*3)))[:8]+"01'"

    print(prev_date)
    print(curr_date)
    print(limit_date)
    
    data_prev, archive, archive, run_parts = download_data(prev_date, curr_date, limit_date) 

    main_fn(run_parts, data_prev, archive, curr_date)

    print("End Time: ", DT.datetime.today())

